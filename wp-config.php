<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'library-book-search-plugin' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'vT:PQGpF,D5n9,Uq[LH*rD=qse+x+Td[f=b+YbsC#(7ZJ@S{s1g<lP6oUL6:n}(>' );
define( 'SECURE_AUTH_KEY',  'GaY):gI+UoFZSieyH[2J5qSR[]1O$s:L4xEy:gFWq>g[y_HV|9y6y68VD};FLYJD' );
define( 'LOGGED_IN_KEY',    'G]>}7hx`T$31W<`XC/7i(+r=dnm+qVz]^9xbL`xLS3x(O:jo%%t$y|06`XtJ*:(I' );
define( 'NONCE_KEY',        'Xr2WzCr^[LzY rPNKghskbA+VF5s=%rRZz@ 8bWQIGiX#okNk9)EFNlp_,0x?Sn+' );
define( 'AUTH_SALT',        'rf0EiO$PLQ&slw5(_~hjhzm`@:H|USAI$!C.Jr*z>I$Sh]2ykmL:XC|[G9~t;M I' );
define( 'SECURE_AUTH_SALT', 'Sb%1OZ~hE-Bi98-6trs{6bM~MLkU5xWP=Yv$5DQeCB-#+|gN8syWyOOH7MRb@gG<' );
define( 'LOGGED_IN_SALT',   'CX,5T2,mmc;_h%o;/+(>%$D^yf}iUL3LSb*^M`cv%.[lP1Qs!yGqgp- +7-u&2v.' );
define( 'NONCE_SALT',       '1*&l>xj@eflQn135b@+}{/Mm}%;(5+w}7lX7k_$x<Hu{[MM3d8aeO}(c0eMkF!c!' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'lbsp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
