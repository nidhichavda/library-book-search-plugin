jQuery("#book-search-form").submit(function( event ) {
    event.preventDefault();
    
    var name = jQuery(this).find('input[name="book_name"]').val();
    var author = jQuery(this).find('input[name="author_name"]').val();
    var publisher = jQuery(this).find('input[name="publisher_name"]').val();
    var rating = jQuery(this).find('input[name="rating"]').val();
    
    jQuery.ajax({
        type : "GET",
        url : ajax_object.ajaxurl,
        data : {
            action : "my_ajax_book_search",
            name : name,
            author : author,
            publisher : publisher,
            rating : rating
        },
        success : function( response ) {
            console.log(response);
        }
    });
})