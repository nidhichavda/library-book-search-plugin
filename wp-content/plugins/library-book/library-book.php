<?php
/**
 * Plugin Name: Library Book Plugin
 * Plugin URI: http://www.mywebsite.com/my-first-plugin
 * Description: The very first plugin that I have ever created.
 * Version: 1.0
 * Author: Your Name
 * Author URI: http://www.mywebsite.com
 */

add_action( 'init', 'register_library_book_post_type' );

function register_library_book_post_type() {

    $labels = array(
        'name'                  => _x( 'Books', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Book', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Books', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Book', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New Book', 'textdomain' ),
        'new_item'              => __( 'New Book', 'textdomain' ),
        'edit_item'             => __( 'Edit Book', 'textdomain' ),
        'view_item'             => __( 'View Book', 'textdomain' ),
        'all_items'             => __( 'All Books', 'textdomain' ),
        'search_items'          => __( 'Search Books', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Books:', 'textdomain' ),
        'not_found'             => __( 'No books found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No books found in Trash.', 'textdomain' ),
    );
    
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'book' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
    );
    
    register_post_type( 'book', $args );

    $labels = array(
        'name' => _x( 'Authors', 'taxonomy general name' ),
        'singular_name' => _x( 'Author', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Authors' ),
        'popular_items' => __( 'Popular Authors' ),
        'all_items' => __( 'All Authors' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Author' ), 
        'update_item' => __( 'Update Author' ),
        'add_new_item' => __( 'Add New Author' ),
        'new_item_name' => __( 'New Author Name' ),
        'menu_name' => __( 'Authors' ),
    ); 
    
    $args = array(
        'labels'             => $labels,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'author' ),
        'hierarchical'       => false
    );
    
    register_taxonomy( 'author', 'book', $args );

    $labels = array(
        'name' => _x( 'Publishers', 'taxonomy general name' ),
        'singular_name' => _x( 'Publisher', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Publishers' ),
        'popular_items' => __( 'Popular Publishers' ),
        'all_items' => __( 'All Publishers' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Publisher' ), 
        'update_item' => __( 'Update Publisher' ),
        'add_new_item' => __( 'Add New Publisher' ),
        'new_item_name' => __( 'New Publisher Name' ),
        'menu_name' => __( 'Publishers' ),
    ); 
    
    $args = array(
        'labels'             => $labels,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'publisher' ),
        'hierarchical'       => false
    );
    
    register_taxonomy( 'publisher', 'book', $args );
}

register_uninstall_hook( __FILE__, 'uninstall_library_book_plugin' );
function uninstall_library_book_plugin() {
    unregister_post_type( 'book' );
}

?>